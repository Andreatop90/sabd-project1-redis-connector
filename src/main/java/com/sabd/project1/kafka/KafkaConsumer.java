package com.sabd.project1.kafka;


import com.sabd.project1.Global;
import org.apache.kafka.clients.consumer.Consumer;

import java.util.Collections;
import java.util.Properties;

public class KafkaConsumer {

    public static Consumer<String, String> createConsumer(String topic) {

        final Properties props = new Properties();

        props.put("bootstrap.servers", Global.BROKERS);
        props.setProperty("zookeeper.connect", Global.ZOOKEEPER);
        props.put("group.id", "lampController");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "100000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        Consumer<String, String> consumer = new org.apache.kafka.clients.consumer.KafkaConsumer<>(props);
        consumer.subscribe(Collections.singleton(topic));
        return consumer;
    }
}
