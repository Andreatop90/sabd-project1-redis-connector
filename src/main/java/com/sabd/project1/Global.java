package com.sabd.project1;

public class Global {

    public static String TOPIC_OUTPUT = "output";
    public static String BROKERS = "localhost:9092";
    public static String ZOOKEEPER = "localhost:2181";
    public static String REDIS_HOST = "localhost";
}
