package com.sabd.project1.redis;

import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Set;

public class RedisClient {

    private Jedis jedis;
    private static RedisClient redisClient = null;

    public RedisClient(String hostNameIp) {
        //Set hostNameIp using properties file/directly(shown below):
        this.jedis = new Jedis(hostNameIp);
        jedis.connect();
    }

    //PATTERN SINGLETON
    public static RedisClient getInstance(String hostnameIP) {
        if (redisClient == null) {
            redisClient = new RedisClient(hostnameIP);
            return redisClient;

        } else {
            return redisClient;
        }
    }

    public ArrayList<String> getAvailableKeys() {
        Set<String> dataFromRedis = jedis.keys("*");
        ArrayList<String> dataFromRedisAsArrayList = new ArrayList<String>(dataFromRedis);
        jedis.close();

        return dataFromRedisAsArrayList;
    }

    public String getValueByName(String keyName) {
        String valueOfKey = jedis.get(keyName);

        jedis.close();

        return valueOfKey;
    }

    public void flushAvailableKeys() {
        jedis.flushAll();
        jedis.close();
    }

    public void insertValue(String key, String value) {
        jedis.set(key, value);
    }

    public String getValue(String key) {
        return jedis.get(key);
    }


}
