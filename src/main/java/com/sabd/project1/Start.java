package com.sabd.project1;

import com.google.gson.Gson;
import com.sabd.project1.kafka.KafkaConsumer;
import com.sabd.project1.model.JsonOuput;
import com.sabd.project1.redis.RedisClient;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;


public class Start {

    public static void main(String[] args) {

        Consumer<String, String> consumer = KafkaConsumer.createConsumer(Global.TOPIC_OUTPUT);
        RedisClient redis = RedisClient.getInstance(Global.REDIS_HOST);

        Gson gson = new Gson();
        //Polling attendendo i risultati di Kafka per inviarli su Redis
        while (true) {
            ConsumerRecords<String, String> consumerRecords = consumer.poll(1000);

            consumerRecords.forEach(record -> {

                JsonOuput output = gson.fromJson(record.value(), JsonOuput.class);


                redis.insertValue(output.getKey(), output.getValue());

                System.out.println("VALUE " + output.getKey() + " ON REDIS:" + redis.getValue(output.getKey()));
            });
            //Alternativa sincrona
            consumer.commitAsync();
        }
    }
}
